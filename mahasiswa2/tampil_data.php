<?php
include('koneksi.php');
$db = new database();
$data_barang = $db->tampil_data();
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <link href="publik/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
            <div>
                <ul class="nav justify-content-around bg-light mb-3">
                <img id="logo" src="publik/img/logo.png" width="200px" />
                    <li class="nav-item">
                    <a class="nav-link text-dark" href="tambah_data.php">Tambah Data</a>
                </ul>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <table class="table table-success table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Makanan</th>
                                    <th scope="col">Stock</th>
                                    <th scope="col">Tahun Exp Date</th>
                                    <th scope="col">Harga</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody class="table-group-divider">
                                <?php
                                $no = 1;
                                foreach($data_barang as $row){
                                ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $row['nama_barang']; ?></td>
                                    <td><?php echo $row['stok']; ?></td>
                                    <td><?php echo $row['harga_beli']; ?></td>
                                    <td><?php echo $row['harga_jual']; ?></td>
                                    <td>
                                        <a href="edit.php?id=<?php echo $row['id_barang']; ?>">
                                        <button type="button" class="btn btn-outline-success">Edit</button>
                                        </a>
                                        <a href="proses_barang.php?action=delete&id=<?php echo $row['id_barang']; ?>">
                                        <button type="button" class="btn btn-outline-danger">Delete</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
    </body>
</html>