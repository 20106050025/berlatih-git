<?php
    
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

     $sheep = new Animal("Shaun");

     echo "Nama animal = " . $sheep->name . "<br>";
     echo "Berkaki = " . $sheep->leg . "<br>";
     echo "Berdarah dingin = " . $sheep->cool_blooded . "<br>";

     echo "<br>";
     echo "<br>";

     $kodok = new Frog("Buduk");
     echo "Nama animal = " . $kodok->name . "<br>";
     echo "Berkaki = " . $kodok->leg . "<br>";
     echo "Berdarah dingin = " . $kodok->cool_blooded . "<br>";
     echo $kodok->jump(); 

    
    echo "<br>";
    echo "<br>";

     $sungokong = new Ape("Kera Sakti");
     echo "Nama animal = " . $sungokong->name . "<br>";
     echo "Berkaki = " . $sungokong->leg . "<br>";
     echo "Berdarah dingin = " . $sungokong->cool_blooded . "<br>";
     echo $sungokong->yell(); 

