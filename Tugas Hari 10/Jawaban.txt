1. Membuat Database :
	CREATE DATABASE myshop;

2. - Tabel Users :
	CREATE TABLE users( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255) );

   - Tabel Categories :
	CREATE TABLE categories( id int(8) AUTO_INCREMENT PRIMARY KEY, name varchar(255) );
	
   - Tabel Items :
	CREATE TABLE items( id int(8) AUTO_INCREMENT PRIMARY KEY, name varchar(255), description varchar(255), price int(10), stock int(8), category_id int(8), FOREIGN KEY(category_id) REFERENCES categories(id) );

3. - Insert Tabel Users :
	INSERT INTO users (name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

   - Insert Tabel Categories :
	INSERT INTO categories(name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

   - Insert Tabel Items :
	INSERT INTO items (name, description, price, stock, category_id) VALUES ("Sumsang B50", "HP Keren Dari Merek Sumsang", 4000000, 100, 1), ("Uniklooh", "Baju Keren dari Brand ternama", 500000, 50,2), ("IMHO Watch:", "Jam Tangan Anak Yang Jujur Banget", 2000000, 10, 1);

4. A. SELECT id, name, email FROM users;
   B. - SELECT * FROM items WHERE price > 1000000;
      - SELECT * FROM items WHERE name like "%sang%";
   C. SELECT items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name AS Kategori FROM items INNER JOIN categories ON items.category_id = categories.id;

5. UPDATE items set price = 2500000 where id = 1;