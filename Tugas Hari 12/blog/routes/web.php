<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@main');
Route::get('/pendaftaran', 'AuthController@form');
Route::post('/kirim', 'AuthController@send');
Route::get('/master', function(){
    return view('adminlteLayout.master');
});
Route::get('/items', function(){
    return view('items.index');
});
Route::get('/items/create', function(){
    return view('items.create');
});
Route::get('/table', function(){
    return view ('items.table');
});
Route::get('/data-tables', function(){
    return view('items.datatable');
});

// CRUD

// 1. Create 
// Untuk Masuk ke Form Cast
Route::get('/cast/create', 'CastController@create');
// Ini untuk mengirim data ke tabel cast
Route::post('/cast', 'CastController@store'); 

// 2. Read
// Menamplkan Semua data tabel casts
Route::get('/cast', 'CastController@index');
// Menampilkan data berdasarkan $id
Route::get('/cast/{id}', 'CastController@show');

// 3. Update
// Masuk ke form, berdasarkan $id
Route::get('/cast/{id}/edit', 'CastController@edit');
// untuk edit data berdasar id. Method PUT
Route::put('/cast/{id}', 'CastController@update');

// 4. Delete
// Delete data berdasarkan id
Route::delete('/cast/{id}', 'CastController@destroy');
