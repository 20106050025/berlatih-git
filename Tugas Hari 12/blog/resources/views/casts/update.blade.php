@extends('adminlteLayout.master')

@section('judul')
    <h1>Ini Halaman Edit Cast</h1>
@endsection

@section('konten')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Form Edit Data Cast</h3>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="/cast/{{$casts->id}}" method="POST">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" placeholder="Masukan Nama" name="nama" value="{{old('nama', $casts->nama)}}">
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" id="umur" placeholder="Masukan Umur" name="umur" value="{{old('umur', $casts->umur)}}">
            </div>
            <div class="form-group">
                <label for="bio">Biografi</label>
                <textarea name="bio" id="bio" cols="30" rows="10" class="form-control"> {{old('bio', $casts->bio )}} </textarea>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
    </form>
</div>
@endsection