@extends('adminlteLayout.master')

@section('judul')
    <h1>Ini Halaman More Casts</h1>
@endsection

@section('konten')
    <a href="/cast" class="btn btn-info btn-sm mb-3">Back</a>
    <h1 class="text-center text-primary">{{$casts->nama}}</h1>
    <h2 class="text-center">Umur : {{$casts->umur}}</h2>
    <h2 class="text-center">Biografi : {{$casts->bio}}</h2>
@endsection