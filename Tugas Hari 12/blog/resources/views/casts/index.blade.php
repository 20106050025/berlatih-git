@extends('adminlteLayout.master')

@section('judul')
    <h1>Ini Halaman Tampilan Data Cast</h1>
@endsection

@section('konten')

<a href="/cast/create" class="btn btn-primary btn-sm mb-3">Add Data</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biografi</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($casts as $key => $item)
          <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-success btn-sm">More</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Update</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
          </tr>
      @empty
          <tr>
            <td>
                Tidak Ada Data di Tabel Ini
            </td>
          </tr>
      @endforelse
    </tbody>
  </table>
@endsection