<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('casts.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required'
            ],
            [
                'nama.required' => 'Sebentar Kak, Nama tidak boleh kosong',
                'umur.required' => 'Sebentar kak, Umur tidak boleh kosong',
                'bio.required' => 'Sebentar kak, Biografi tidak boleh kosong',
            ]
        );

        DB::table('casts')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );
        return redirect('/cast');
    }

    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('casts.index', compact('casts'));
    }

    public function show($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        return view('casts.more', compact('casts'));
    }

    public function edit($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        return view('casts.update', compact('casts'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required'
            ],
            [
                'nama.required' => 'Sebentar Kak, Nama tidak boleh kosong',
                'umur.required' => 'Sebentar kak, Umur tidak boleh kosong',
                'bio.required' => 'Sebentar kak, Biografi tidak boleh kosong',
            ]
        );

        DB::table('casts')
        ->where('id', $id)
        ->update(
            [
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
            ]
        );
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
    
}
