<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function send(Request $request){
        // dd($request->all());
        $firstname = $request['namadepan'];
        $lastname = $request['namabelakang'];

        return view('selamat', compact('firstname','lastname'));
    }
}
